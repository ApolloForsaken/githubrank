package io.githubrank

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import io.githubrank.GithubOrganizationClient.{OrganizationContributorsResponseFailure, OrganizationContributorsResponseSuccess, GetOrganizationContributions}
import io.githubrank.GithubRepositoryClient.Contributor
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import spray.json._

class GithubRankRouteSpec extends FlatSpec with ScalatestRouteTest with Matchers with BeforeAndAfterAll {

  behavior of "GithubRank Organization Route"

  val testKit: ActorTestKit = ActorTestKit()
  override def afterAll(): Unit = testKit.shutdownTestKit()

  val organization = "SuperOrg"

  it should "Serialize result correctly" in {

    val mockedBehavior: Behaviors.Receive[GetOrganizationContributions] = Behaviors.receiveMessage[GithubOrganizationClient.GetOrganizationContributions] { msg =>
      assert(msg.organization == organization)
      msg.replyTo ! OrganizationContributorsResponseSuccess(List(Contributor("John", 33), Contributor("Jack", 22)))
      Behaviors.same
    }

    val mockedGithubOrganizationClient: ActorRef[GetOrganizationContributions] = testKit.spawn(mockedBehavior)

    val route: Route = new GithubRankRoute(mockedGithubOrganizationClient)(testKit.system).contributorsRoute

    val expectedResult = "[{\"contributions\":33,\"name\":\"John\"},{\"contributions\":22,\"name\":\"Jack\"}]".parseJson

    Get(s"/org/$organization/contributors") ~> route ~> check {
      responseAs[String].parseJson shouldEqual expectedResult
    }

  }

  it should "Return with 404 if organization was not found" in {

    val mockedBehavior: Behaviors.Receive[GetOrganizationContributions] = Behaviors.receiveMessage[GithubOrganizationClient.GetOrganizationContributions] { msg =>
      assert(msg.organization == organization)
      msg.replyTo ! OrganizationContributorsResponseFailure(StatusCodes.NotFound, "Organization not found")
      Behaviors.same
    }

    val mockedGithubOrganizationClient: ActorRef[GetOrganizationContributions] = testKit.spawn(mockedBehavior)

    val route: Route = new GithubRankRoute(mockedGithubOrganizationClient)(testKit.system).contributorsRoute

    Get(s"/org/$organization/contributors") ~> route ~> check {
      status shouldEqual StatusCodes.NotFound
    }

  }

}
