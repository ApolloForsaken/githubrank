package io.githubrank

import org.scalatest.FlatSpec

class GithubOrganizationClientSpec extends FlatSpec {

  behavior of "GithubOrganizationClient#parseLinkHeader"

  it should "parse header correctly" in {

    val headerLink = "<https://api.github.com/organizations/6207831/repos?page=2>; rel=next, <https://api.github.com/organizations/6207831/repos?page=3>; rel=last"
    val expectedResponse = "https://api.github.com/organizations/6207831/repos?page=2"

    assertResult(Some(expectedResponse))(GithubOrganizationClient.parseLinkHeader(headerLink))

  }

  it must "not throw exception if header is empty" in {

    val headerLink = ""

    assertResult(None)(GithubOrganizationClient.parseLinkHeader(headerLink))

  }

}
