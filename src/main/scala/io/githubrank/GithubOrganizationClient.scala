package io.githubrank

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.util.Timeout
import io.githubrank.GithubRepositoryClient.{Contributor, GetContributors, RepositoryContributorsResponseFailure, RepositoryContributorsResponseSuccess}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

/**
 * HTTP Client for Github API organizations
 *
 * @see [[https://developer.github.com/v3/orgs/]]
 */
object GithubOrganizationClient extends GithubClient {

  sealed trait OrganizationMessage

  final case class GetOrganizationContributions(organization: String, replyTo: ActorRef[OrganizationResponse]) extends OrganizationMessage

  final case class GetRepositoryContributors(repository: Repository) extends OrganizationMessage

  // Github Http Responses
  final case class HttpResponseSuccess(repositories: List[Repository], nextPageUrl: Option[String]) extends OrganizationMessage

  final case class HttpResponseFailure(statusCode: StatusCode, reason: String) extends OrganizationMessage

  // Repository Client Responses
  final case class RepositoryClientResponseSuccess(contributors: List[Contributor]) extends OrganizationMessage

  final case class RepositoryClientResponseFailure(statusCode: StatusCode, reason: String) extends OrganizationMessage

  // Responses
  sealed trait OrganizationResponse

  final case class OrganizationContributorsResponseSuccess(contributors: List[Contributor]) extends OrganizationResponse

  final case class OrganizationContributorsResponseFailure(status: StatusCode, reason: String) extends OrganizationResponse

  // DTOs
  final case class Repository(name: String, contributors_url: String)

  def apply(): Behavior[OrganizationMessage] =
    Behaviors.setup { ctx =>

      val repositoryClient = ctx.spawn(GithubRepositoryClient(), "Repository")

      import akka.actor.typed.scaladsl.adapter._
      implicit val system: ActorSystem[Nothing] = ctx.system
      implicit val executionContext: ExecutionContextExecutor = ctx.system.dispatchers.lookup(DispatcherSelector.fromConfig(CONFIG_API_DISPATCHER))

      // TODO: remove duplication with contributorsRequest
      def repositoriesRequest(repositoriesUrl: String): Unit = {
        val request = Http(system.toClassic).singleRequest(HttpRequest(
          uri = repositoriesUrl,
          headers = GITHUB_ACCESS_TOKEN.map(token => List(GithubAuthorizationHeader(token))).getOrElse(List.empty),
          entity = HttpEntity(ContentTypes.`application/json`, "data")
        )).transformWith {
          case Success(response) =>
            response.status match {
              case StatusCodes.OK =>
                val nextPage: Option[String] = response.headers
                  .find(_.name == GITHUB_API_HEADER_LINK)
                  .map(_.value)
                  .flatMap(parseLinkHeader)
                Unmarshal(response).to[List[Repository]] map { repositories => HttpResponseSuccess(repositories, nextPage) }
              case statusCode =>
                Unmarshal(response).to[String] map { reason => HttpResponseFailure(statusCode, reason) }
            }
          case Failure(t) => Future(HttpResponseFailure(StatusCodes.InternalServerError, t.getMessage))
        }

        ctx.pipeToSelf(request) {
          case Success(response) => response
          case Failure(t) => HttpResponseFailure(StatusCodes.InternalServerError, t.getMessage)
        }
      }

      def waiting(): Behaviors.Receive[OrganizationMessage] = Behaviors.receiveMessage {
        case GetOrganizationContributions(organization, replyTo) =>
          ctx.log.info(s"Searching repositories of $organization")

          val repositoriesUrl = githubOrganizationRepositoriesUrl(organization)

          repositoriesRequest(repositoriesUrl)

          gettingRepositories(List.empty, replyTo)

        case msg =>
          ctx.log.warn(s"Received wrong message $msg")
          Behaviors.same
      }

      def gettingRepositories(repositories: List[Repository], replyTo: ActorRef[OrganizationResponse]): Behaviors.Receive[OrganizationMessage] = Behaviors.receiveMessage {

        // TODO: remove duplication
        // We will reject any other requests to avoid concurrent requests to Github API
        case GetOrganizationContributions(organization, replyTo) =>
          val msg = s"Rejecting request for $organization because another request is already in progress. Please try again later"
          ctx.log.warn(msg)
          replyTo ! OrganizationContributorsResponseFailure(StatusCodes.TooManyRequests, msg)
          Behaviors.same

        case HttpResponseSuccess(newRepositories, nextPage) =>
          val mergedRepositories = repositories ++ newRepositories

          nextPage match {
            case Some(nextPage) =>
              repositoriesRequest(nextPage)
              gettingRepositories(mergedRepositories, replyTo)
            case None =>
              ctx.log.info(s"Found ${mergedRepositories.length} Repositories")
              ctx.self ! GetRepositoryContributors(mergedRepositories.head)
              gettingContributors(List.empty, mergedRepositories.tail, replyTo)
          }

        case HttpResponseFailure(statusCode, reason) =>
          ctx.log.error(s"Request failed, $statusCode -  $reason")
          replyTo ! OrganizationContributorsResponseFailure(statusCode, reason)
          waiting()

        case msg =>
          ctx.log.warn(s"Received wrong message $msg")
          Behaviors.same
      }

      def gettingContributors(contributors: List[Contributor], repositories: List[Repository], replyTo: ActorRef[OrganizationResponse]): Behaviors.Receive[OrganizationMessage] = Behaviors.receiveMessage {

        // TODO: remove duplication
        // We will reject any other requests to avoid concurrent requests to Github API
        case GetOrganizationContributions(organization, replyTo) =>
          val msg = s"Rejecting request for $organization because another request is already in progress. Please try again later"
          ctx.log.warn(msg)
          replyTo ! OrganizationContributorsResponseFailure(StatusCodes.TooManyRequests, msg)
          Behaviors.same

        case GetRepositoryContributors(repository) =>

          implicit val timeout: Timeout = 60.seconds

          ctx.ask[GetContributors, GithubRepositoryClient.RepositoryResponse](repositoryClient, ref => GetContributors(repository.contributors_url, ref)) {
            case Success(response) => response match {
              case RepositoryContributorsResponseSuccess(contributors) => RepositoryClientResponseSuccess(contributors)
              case RepositoryContributorsResponseFailure(statusCode, reason) => RepositoryClientResponseFailure(statusCode, reason)
            }
            case Failure(t) => RepositoryClientResponseFailure(StatusCodes.InternalServerError, t.getMessage)
          }

          Behaviors.same

        case RepositoryClientResponseSuccess(newContributors) =>
          val contributorsMerged = contributors ::: newContributors

          repositories match {
            case Nil =>
              val sortedContributors = contributorsMerged.groupMap(_.login)(_.contributions).view.mapValues(_.sum)
                .toList
                .sortBy(_._2)
                .reverse
                .map { case (k, v) => Contributor(k, v) }

              ctx.log.info("Returning result")
              replyTo ! OrganizationContributorsResponseSuccess(sortedContributors)
              waiting()
            case nextRepository :: restRepositories =>
              ctx.self ! GetRepositoryContributors(nextRepository)
              gettingContributors(contributorsMerged, restRepositories, replyTo)
          }

        case RepositoryClientResponseFailure(statusCode, reason) =>
          ctx.log.error(s"Request failed, $statusCode -  $reason")
          replyTo ! OrganizationContributorsResponseFailure(statusCode, reason)
          waiting()

        case msg =>
          ctx.log.warn(s"Received wrong message $msg")
          Behaviors.same
      }

      waiting()
    }

  private def githubOrganizationRepositoriesUrl(organization: String): String = s"$GITHUB_API_URL/orgs/$organization/repos"

}
