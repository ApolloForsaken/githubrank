package io.githubrank

import akka.actor.typed.ActorSystem

object GithubRankApp extends App {

  val system: ActorSystem[GithubRankServer.Message] = ActorSystem(GithubRankServer("localhost", 8080), "BuildJobsServer")

}
