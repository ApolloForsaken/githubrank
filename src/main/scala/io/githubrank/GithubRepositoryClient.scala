package io.githubrank

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, DispatcherSelector}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

/**
 * HTTP Client for Github API repositories
 *
 * @see [[https://developer.github.com/v3/repos/]]
 */
object GithubRepositoryClient extends GithubClient {

  sealed trait RepositoryMessage

  final case class GetContributors(contributorsUrl: String, replyTo: ActorRef[RepositoryResponse]) extends RepositoryMessage

  // Github Http Responses
  final case class HttpResponseSuccess(contributors: List[Contributor], nextPageUrl: Option[String]) extends RepositoryMessage

  final case class HttpResponseFailure(statusCode: StatusCode, reason: String) extends RepositoryMessage

  // Responses
  sealed trait RepositoryResponse

  final case class RepositoryContributorsResponseSuccess(contributors: List[Contributor]) extends RepositoryResponse

  final case class RepositoryContributorsResponseFailure(status: StatusCode, reason: String) extends RepositoryResponse

  // DTOs
  final case class Contributor(login: String, contributions: Int)

  def apply(): Behavior[RepositoryMessage] =
    Behaviors.setup { ctx =>

      import akka.actor.typed.scaladsl.adapter._
      implicit val system: ActorSystem[Nothing] = ctx.system
      implicit val executionContext: ExecutionContextExecutor = ctx.system.dispatchers.lookup(DispatcherSelector.fromConfig(CONFIG_API_DISPATCHER))

      // TODO: remove duplication with repositoriesRequest
      def contributorsRequest(repositoriesUrl: String): Unit = {
        val request = Http(system.toClassic).singleRequest(HttpRequest(
          uri = repositoriesUrl,
          headers = GITHUB_ACCESS_TOKEN.map(token => List(GithubAuthorizationHeader(token))).getOrElse(List.empty),
          entity = HttpEntity(ContentTypes.`application/json`, "data")
        )).transformWith {
          case Success(response) =>
            response.status match {
              case StatusCodes.OK =>
                val nextPage: Option[String] = response.headers
                  .find(_.name == GITHUB_API_HEADER_LINK)
                  .map(_.value)
                  .flatMap(parseLinkHeader)
                Unmarshal(response).to[List[Contributor]] map { contributors => HttpResponseSuccess(contributors, nextPage) }
              case statusCode =>
                Unmarshal(response).to[String] map { reason => HttpResponseFailure(statusCode, reason) }
            }
          case Failure(t) => Future(HttpResponseFailure(StatusCodes.InternalServerError, t.getMessage))
        }

        ctx.pipeToSelf(request) {
          case Success(response) => response
          case Failure(t) => HttpResponseFailure(StatusCodes.InternalServerError, t.getMessage)
        }
      }

      def waiting(): Behaviors.Receive[RepositoryMessage] = Behaviors.receiveMessage {
        case GetContributors(contributionsUrl, replyTo) =>
          ctx.log.info(s"Searching contributors of $contributionsUrl")

          contributorsRequest(contributionsUrl)

          working(List.empty, replyTo)

        case msg =>
          ctx.log.warn(s"Received wrong message $msg")
          Behaviors.same
      }

      def working(contributors: List[Contributor], replyTo: ActorRef[RepositoryResponse]): Behaviors.Receive[RepositoryMessage] = Behaviors.receiveMessage {

        case HttpResponseSuccess(newContributors, nextPage) =>
          val mergedContributors = contributors ::: newContributors

          nextPage match {
            case Some(nextPage) =>
              contributorsRequest(nextPage)
              working(mergedContributors, replyTo)
            case None =>
              ctx.log.info(s"Found ${mergedContributors.length} Contributors")
              replyTo ! RepositoryContributorsResponseSuccess(mergedContributors)
              waiting()
          }

        case HttpResponseFailure(statusCode, reason) =>
          ctx.log.error(s"Request failed, $statusCode -  $reason")
          replyTo ! RepositoryContributorsResponseFailure(statusCode, reason)
          waiting()

        case msg =>
          ctx.log.warn(s"Received wrong message $msg")
          Behaviors.same
      }

      waiting()
    }

}
