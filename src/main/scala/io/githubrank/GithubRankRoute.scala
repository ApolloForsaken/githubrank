package io.githubrank

import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.caching.LfuCache
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpResponse, StatusCodes, Uri}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, RequestContext, Route, RouteResult}
import akka.util.Timeout
import io.githubrank.GithubOrganizationClient.{OrganizationContributorsResponseFailure, OrganizationContributorsResponseSuccess, OrganizationResponse}
import io.githubrank.GithubRepositoryClient.Contributor
import spray.json.{DefaultJsonProtocol, RootJsonFormat}
import akka.http.scaladsl.server.directives.CachingDirectives._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import akka.actor.typed.scaladsl.adapter._
import akka.http.caching.scaladsl.Cache
import io.githubrank.GithubRankRoute.RateLimitException


/**
 * Github Rank Route for `/org/$organization/contributors`
 */
class GithubRankRoute(githubOrganizationClient: ActorRef[GithubOrganizationClient.GetOrganizationContributions])(implicit system: ActorSystem[Nothing]) extends SprayJsonSupport with DefaultJsonProtocol {

  import akka.actor.typed.scaladsl.AskPattern._

  implicit val contributorJsonFormat: RootJsonFormat[Contributor] = jsonFormat(Contributor, fieldName1 = "name", fieldName2 = "contributions")

  implicit val timeout: Timeout = 300.seconds

  val contributorsCache: Cache[Uri, RouteResult] = LfuCache.apply(system.toClassic)
  val keyerFunction: PartialFunction[RequestContext, Uri] = {
    case r: RequestContext => r.request.uri
  }

  implicit def exceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case RateLimitException(reason) =>
        complete(HttpResponse(StatusCodes.TooManyRequests, entity = reason))
    }

  lazy val contributorsRoute: Route = Route.seal(
    alwaysCache(contributorsCache, keyerFunction) {
      path("org" / Segment / "contributors") {
        organization =>
          get {
            val getContributors: Future[OrganizationResponse] = githubOrganizationClient.ask(GithubOrganizationClient.GetOrganizationContributions(organization, _))
            onComplete(getContributors) {
              case Success(result) => result match {
                case OrganizationContributorsResponseSuccess(contributors) => complete(contributors)
                case OrganizationContributorsResponseFailure(status, reason) => status match {
                  // This is to avoid caching the response.
                  case StatusCodes.TooManyRequests => failWith(RateLimitException(reason))
                  case _ => complete(status, reason)
                }
              }
              case Failure(reason) => complete(StatusCodes.InternalServerError -> reason)
            }
          }
      }
    })

}

object GithubRankRoute {

  final case class RateLimitException(msg: String) extends Exception(msg)

}
