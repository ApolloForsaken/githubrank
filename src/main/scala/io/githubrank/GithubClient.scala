package io.githubrank

import akka.http.scaladsl.model.headers.CustomHeader

trait GithubClient extends JsonSupport {

  final val GITHUB_API_URL = "https://api.github.com"
  final val GITHUB_API_HEADER_LINK = "Link"

  final val ENVIRONMENTAL_VARIABLE_GITHUB_TOKEN = "GH_TOKEN"
  final val GITHUB_ACCESS_TOKEN: Option[String] = sys.env.get(ENVIRONMENTAL_VARIABLE_GITHUB_TOKEN)

  final val CONFIG_API_DISPATCHER = "github-api-dispatcher"

  case class GithubAuthorizationHeader(token: String) extends CustomHeader {
    override def name(): String = "Authorization"

    override def value(): String = s"token $token"

    override def renderInRequests(): Boolean = true

    override def renderInResponses(): Boolean = false
  }

  private[githubrank] def parseLinkHeader(header: String): Option[String] = {
    header
      .split(",")
      .find(_.endsWith("rel=next"))
      .map(str => str.substring(str.indexOf('<') + 1, str.indexOf('>')))
  }

}
