package io.githubrank

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import io.githubrank.GithubOrganizationClient.Repository
import io.githubrank.GithubRepositoryClient.Contributor
import spray.json._

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val repoJsonFormat: RootJsonFormat[Repository] = jsonFormat2(Repository)
  implicit val contributorJsonFormat: RootJsonFormat[Contributor] = jsonFormat2(Contributor)

}
