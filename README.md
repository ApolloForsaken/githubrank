# GithubRank Challenge
Simple Client to retrieve a sorted list contributors of of a Github Organization.

Akka Application with Akka-HTTP used for both server and client.

You can use environmental variable `GH_TOKEN` with your Github access token to avoid Github API rate limits for non-authorized calls.

## Getting Started
This is an sbt project [sbt](https://www.scala-lang.org/documentation/getting-started-sbt-track/getting-started-with-scala-and-sbt-on-the-command-line.html)

### Build
Using [sbt-assembly](https://github.com/sbt/sbt-assembly) 
- Run from terminal (as administrator):
> sbt assembly

### Run (Ubuntu)
> GH_TOKEN=qqqqqqqqq; java -jar target/scala-2.13/githubrank-assembly-1.0.jar 

## Endpoints
Akka Http Server Port: 8080

#### Organization Contributors
- GET "/org/{organization}/contributors" 

Sample Request using HTTPie
> http --timeout=300 localhost:8080/org/ScalaConsultants/contributors

##### Sample Response
[
    {
        "contributions": 974,
        "name": "alexott"
    },
    {
        "contributions": 351,
        "name": "kushti"
    }
]

- Status `404` will be returned if no organization is found.
- Status `429` will be returned if there is another request in progress. This was done to avoid concurrent requests to Github API.

